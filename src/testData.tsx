import img1 from "./img/1.jpg";
import img2 from "./img/2.jpg";
import img3 from "./img/3.jpg";
import img5 from "./img/5.jpg";
import img4 from "./img/4.jpg";
import img6 from "./img/6.jpg";
import img7 from "./img/7.jpg";
import { Player } from "./types";

export const PLAYERS: Player[] = [
  { id: "Eduardo_0", nombre: "Eduardo", img: img1, isCapitan: false },
  { id: "Daniel_1", nombre: "Daniel", img: img2, isCapitan: false },
  { id: "Pedro_2", nombre: "Pedro", img: img3, isCapitan: false },
  { id: "Alicia_3", nombre: "Alicia", img: img4, isCapitan: false },
  { id: "Katherine_4", nombre: "Katherine", img: img5, isCapitan: false },
  { id: "ElCapi_5", nombre: "ElCapi", img: img6, isCapitan: false },
  { id: "Diego_6", nombre: "Diego", img: img7, isCapitan: false },
];
