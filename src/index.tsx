import React from "react";
import ReactDOM from "react-dom";

import "./index.css";
import { PLAYERS } from "./testData";
import { TeamEditor } from "./components/TeamEditor";

const App = () => {
  return (
    <div className="intentoBody">
      <div className="header">
        <h1>Seleccionador poderoso de equipos</h1>
        <hr />
      </div>

      <TeamEditor playerPool={PLAYERS} />
    </div>
  );
};

ReactDOM.render(<App />, document.getElementById("root"));
