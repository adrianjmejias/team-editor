

export type PlayerRole = "defensa" | "medio-campo" | "delantero"

export type Player = {
  id: string;
  nombre: string;
  img: string;
  role?: PlayerRole;
  isCapitan: boolean;
};

export type Team = {
  captain: Player
  players: Player[]
  name: string
}

export type PickTurn = "TeamA" | "TeamB"