import React from "react";
import { Player } from "../../types";

export interface PlayerCardProps {
  onClick?(player: Player): void;
  player: Player;
}

export const PlayerCard: React.FC<PlayerCardProps> = ({ player, onClick }) => {
  
  return (
    <div className="cont-player" onClick={onClick?.bind(null, player)}>
      <div className="cont-image">
        CAPITNA
        <img src={player.img} alt="Player" />
      </div>
      <div className="cont-description">
        <p>{player.nombre}</p>
      </div>
    </div>
  );
};

export default PlayerCard