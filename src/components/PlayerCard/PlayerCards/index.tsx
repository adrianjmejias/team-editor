import React from "react";
import { PlayerCard } from "..";
import { Player } from "../../../types";


export interface PlayerCardsProps{
    players: Player[]
    onClick?(player: Player): void
}

export const PlayerCards: React.FC<PlayerCardsProps> = ({players, onClick}) => {
  return (
    <div className="cont-teams team">
      {players.map((player) => (
        <PlayerCard
          key={player.id}
          player={player}
          onClick={onClick}
        />
      ))}
    </div>
  );
};

export default PlayerCards;
