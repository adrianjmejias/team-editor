import React from "react";
import { useTeamEditorContext } from "../TeamEditorContextProvider";
import PlayerCards from "../../PlayerCard/PlayerCards";

export interface PoolCardProps {}

export const PoolCard: React.FC<PoolCardProps> = () => {
  const { pool, addPlayer } = useTeamEditorContext();

  return <PlayerCards players={pool} onClick={addPlayer} />;
};

export default PoolCard
