import React from "react";
import { Player } from "../../types";
import { PoolCard } from "./PoolCard";
import TeamCard from "./TeamCard";
import TeamEditorContextProvider, {
  useTeamEditorContext,
} from "./TeamEditorContextProvider";
import styles from './team-editor.module.css'


const TeamEditorContent: React.FC = () => {
  const { teamA, teamB } = useTeamEditorContext();

  return (
    <div className={styles.teamEditor}>
      <TeamCard players={teamA} />
      <TeamCard players={teamB} />
      <PoolCard />
    </div>
  );
};

export interface TeamEditorProps {
  playerPool: Player[];
}

export const TeamEditor: React.FC<TeamEditorProps> = ({ playerPool }) => {
  return (
    <TeamEditorContextProvider playerPool={playerPool}>
      <TeamEditorContent />
    </TeamEditorContextProvider>
  );
};
