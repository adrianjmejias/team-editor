import React from "react";
import { useTeamEditorContext } from "../TeamEditorContextProvider";
import PlayerCards, { PlayerCardsProps } from "../../PlayerCard/PlayerCards";

interface TeamCardProps extends PlayerCardsProps {}

export const TeamCard: React.FC<TeamCardProps> = ({ players }) => {
  const { removePlayer } = useTeamEditorContext();

  return <PlayerCards players={players} onClick={(p) => removePlayer(p.id)} />;
};

export default TeamCard;
