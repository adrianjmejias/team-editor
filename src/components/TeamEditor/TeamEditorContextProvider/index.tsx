import React from "react";
import { PickTurn, Player } from "../../../types";

export interface ITeamEditorContext {
  pool: Player[];
  teamA: Player[];
  teamB: Player[];

  turn: PickTurn;
  addPlayer(player: Player): void;
  removePlayer(id: string): void;
}

const TeamEditorContext = React.createContext<ITeamEditorContext>(null!);

export interface TeamEditorProps {
  playerPool: Player[];
}

export const TeamEditorContextProvider: React.FC<TeamEditorProps> = ({
  playerPool,
  children,
}) => {
  const [turn, setTurn] = React.useState<PickTurn>("TeamA");
  const [pool, setPool] = React.useState(playerPool);
  const [teamA, setTeamA] = React.useState<Player[]>([]);
  const [teamB, setTeamB] = React.useState<Player[]>([]);

  const currentTeamSetter = React.useMemo(
    () => (turn === "TeamB" ? setTeamB : setTeamA),
    [turn]
  );

  const passTurn = React.useCallback(() => {
    setTurn((oldTurn) => (oldTurn === "TeamA" ? "TeamB" : "TeamA"));
  }, []);

  const addPlayer = React.useCallback(
    (player: Player) => {
      setPool((pool) => pool.filter((p) => p.id !== player.id));
      currentTeamSetter((team) => [...team, player]);
      passTurn();
    },
    [currentTeamSetter, passTurn]
  );

  const removePlayer = React.useCallback(
    (id: string) => {
      currentTeamSetter((team) => {
        const playerToDeleteIndex = team.findIndex(
          (player) => player.id === id
        );

        if (playerToDeleteIndex >= 0) {
          const playerDeleted = team[playerToDeleteIndex];

          setPool((oldPool) => [...oldPool, playerDeleted!]);

          return team.filter((p) => p.id !== playerDeleted.id);
        }

        return team;
      });
    },
    [currentTeamSetter]
  );

  return (
    <TeamEditorContext.Provider
      value={{
        turn,
        pool,
        addPlayer,
        removePlayer,
        teamA,
        teamB,
      }}
    >
      {children}
    </TeamEditorContext.Provider>
  );
};

export const useTeamEditorContext = () => {
  const context = React.useContext(TeamEditorContext);

  if (!context) {
    throw new Error(
      "useTeamEditorContext must be used within a TeamEditorContextProvider"
    );
  }

  return context;
};

export default TeamEditorContextProvider;
